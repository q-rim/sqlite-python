#! /usr/bin/python
import sqlite3

def Main():

	try:
		connection = sqlite3.connect('database/test.db')	# create connection to databse, "test.db"
		curser = connection.cursor()			# create a curser - used to execute our statement
		curser.executescript("""DROP TABLE IF EXISTS Pets;
					CREATE TABLE Pets(Id INT, Name TEXT, Price INT);
					INSERT INTO Pets VALUES(1, 'Cat', 400);
					INSERT INTO Pets VALUES(2, 'Dog', 600);""")
		
		pets = ((3, 'Rabbit', 200), 
			(4, 'Bird', 60),
			(5, 'Goat', 500))

		curser.executemany("INSERT INTO Pets VALUES(?, ?, ?)", pets)	# executes many queries given the array or dictionary		
		connection.commit()

		curser.execute("SELECT * FROM Pets")
								# curser holds result of the query
		data = curser.fetchall()			# to get the result, get the first row of the database

		for row in data:
			print row

		print "SQLite version: " + str(data)
	

	# ERROR HANDLING
	except sqlite3.Error, e:
		if connection:				# if there is a connection open
			connection.rollback()		# roll back any changes
			print "ERROR:  There was a problem with SQL"
	
	finally:
		if connection:
			connection.close()

if __name__ == '__main__':
	Main ()

